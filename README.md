# Outlined Text

Apply multiple outlines/borders to text widget

<img src="https://gitlab.com/jorgesanure-pub-dep/outlined-text/-/raw/master/assets/preview.jpeg" height='300px'>

[DEMO](https://dartpad.dartlang.org/1183492137bc71537b24a4a08947fbc4)

```dart
OutlinedText(
    text: Text('Outlined Text...', 
        style: TextStyle(
            color: Colors.black, 
            fontSize:75
        )
    ),
    strokes: [
        OutlinedTextStroke(
            color: Colors.amber, 
            width: 5
        ),
        OutlinedTextStroke(
            color: Colors.red, 
            width: 8
        ),
        OutlinedTextStroke(
            color: Colors.blue, 
            width: 8
        ),
    ],
)
```
