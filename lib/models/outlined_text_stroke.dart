import 'package:flutter/material.dart';

class OutlinedTextStroke {
  final Color? color;
  final double? width;

  OutlinedTextStroke({this.color, this.width});
}
