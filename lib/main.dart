import 'package:flutter/material.dart';

import 'outlined_text.dart';

void main() => runApp(MaterialApp(
        home: Scaffold(
            body: Center(
                child: OutlinedText(
      text: Text('Outlined Text...',
          style: TextStyle(color: Colors.black, fontSize: 75)),
      strokes: [
        OutlinedTextStroke(color: Colors.amber, width: 5),
        OutlinedTextStroke(color: Colors.red, width: 8),
        OutlinedTextStroke(color: Colors.blue, width: 8),
      ],
    )))));
